/*
=====================================
======= KASIR SEDERHANA =============
=====================================

Proses nya :
Input harga barang
input jumlah barang
output total belanja
Input jumlah bayar
output kembalian
 */
import java.util.Scanner;
public class KasirSederhana {
    public static void main(String[] args) {
        System.out.println("=====================================\n" +
                "======= KASIR SEDERHANA =============\n" +
                "=====================================");
        Scanner scn = new Scanner(System.in);
        System.out.println("Masukan Harga Barang : ");
        long harga = scn.nextLong();
        System.out.println("Masukan Jumlah Barang : ");
        int jumlah = scn.nextInt();
        long total = harga * jumlah;
        System.out.println("Total Belanja : " + total);

        System.out.println("Masukan Jumlah Bayar : ");
        long bayar = scn.nextLong();
        long kembali = bayar - total;
        System.out.println("Total Kembalian : " + kembali);
    }
}